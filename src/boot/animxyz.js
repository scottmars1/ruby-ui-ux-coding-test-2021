import { boot } from 'quasar/wrappers'

import VueAnimXyz from '@animxyz/vue3'

// "async" is optional;
// more info on params: https://v2.quasar.dev/quasar-cli/boot-files
export default boot(({ app }) => {
  app.use(VueAnimXyz)
})
