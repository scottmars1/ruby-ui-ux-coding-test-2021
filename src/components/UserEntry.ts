export class UserEntry {
    id: number
    rank: number
    name: string
    date: Date
    points: number

    constructor(id: number, rank: number, name: string, date: Date, points: number) {
        this.id = id;
        this.rank = rank;
        this.name = name;
        this.date = date;
        this.points = points;
    }
}