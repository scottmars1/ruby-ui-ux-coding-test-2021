// https://github.com/vuejs/vue-next/issues/3130
declare module '*.vue' {
    import { ComponentOptions } from 'vue'
    const component: ComponentOptions
    export default component
}